document.addEventListener('DOMContentLoaded', function () {
    const headline = document.getElementById('headline')
    const name = document.getElementById('name')
    const content = document.getElementById('content')
    const contact = document.getElementById('contact')
    const tel = document.getElementById('tel')

    const fileButton = document.getElementById('file')
    const image = document.getElementById('image')
    const placeholder = document.getElementById('placeholder')

    const helper = document.getElementById('helper')
    const close = document.getElementById('close')

    // LocalStorage
    const storable = [headline, name, content, tel, contact]
    storable.forEach(function (element) {
        // Restore
        var value = localStorage.getItem(element.id)
        if (value) {
            if (element.nodeName === 'INPUT') {
                element.value = value
            }
            if (element.nodeName === 'DIV') {
                element.innerHTML = value
            }
        }

        // Save
        element.addEventListener('input', function (e) {
            localStorage.setItem(e.target.id, element.nodeName === 'INPUT' ? e.target.value : e.target.innerHTML)
        })
    })

    // Set user language for hypenation
    document.documentElement.setAttribute('lang', navigator.language.split('-')[0])

    // Set document title to headline: name
    headline.addEventListener('input', changeTitle)
    name.addEventListener('input', changeTitle)

    // Resize text area
    content.addEventListener('input', function (e) {
        resizeContent(e.target)
    })

    // Close helper
    close.addEventListener('click', function () {
        helper.style.display = 'none'
    })

    // Add qrcode
    const qrcode = new QRCode(document.getElementById('qrcode'), {
        text: 'tel:' + tel.value,
        width: 160,
        height: 160
    })

    // Generate qrcode
    tel.addEventListener('input', function (e) {
        qrcode.makeCode('tel:' + e.target.value)
    })

    // Add image
    fileButton.addEventListener('change', function (e) {
        var file = e.target.files[0]

        image.file = file

        var reader = new FileReader()
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result
                resizeContent(content)
                image.style.display = 'block'
                placeholder.style.display = 'none'
            }
        })(image)
        reader.readAsDataURL(file)
    })

    // Hide image
    image.addEventListener('click', function () {
        image.style.display = 'none'
        placeholder.style.display = 'flex'
    })

    let fontSize = parseInt(content.style.fontSize) || 48
    function resizeContent(element) {
        while (element.scrollHeight === element.clientHeight && fontSize <= 48) {
            fontSize = fontSize + 0.25
            element.style.fontSize = fontSize + 'px'
        }
        while (element.scrollHeight > element.clientHeight) {
            fontSize = fontSize - 0.25
            element.style.fontSize = fontSize + 'px'
        }
    }

    function changeTitle() {
        document.title = headline.value + ': ' + name.value
    }
})
