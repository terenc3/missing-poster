# SPDX-FileCopyrightText: 2023 Benjamin Kahlau
#
# SPDX-License-Identifier: GPL-3.0-or-later
NODE := node
NPM := npm
BIN := ./node_modules/.bin
ROLLUP := $(BIN)/rollup
ROLLUP_FLAGS := --format iife --sourcemap true
POSTCSS := $(BIN)/postcss
TERSER := $(BIN)/terser
TERSER_FLAGS :=
POSTHTML := $(BIN)/posthtml
POSTHTML_FLAGS := -c .posthtmlrc
PRETTIER := $(BIN)/prettier
PRETTIER_FLAGS := --cache --cache-strategy metadata --write
ESLINT := $(BIN)/eslint
ESLINT_FLAGS := --fix --ignore-path .gitignore

DIST := dist
SRC := src

SRC_HTML := $(wildcard $(SRC)/*.html)
SRC_JS := $(wildcard $(SRC)/*.js)
SRC_SCSS := $(wildcard $(SRC)/*.scss)
SRC_MD := $(wildcard *.md)

all: node_modules format lint $(DIST)/index.html

$(DIST)/missing-poster.js: $(SRC)/missing-poster.js node_modules
	$(ROLLUP) $< $(ROLLUP_FLAGS) --file $@

$(DIST)/missing-poster.min.js: $(DIST)/missing-poster.js node_modules
	$(TERSER) $< $(TERSER_FLAGS) --source-map "url=${notdir $@}.map" -o $@

$(DIST)/missing-poster.min.css: $(SRC)/missing-poster.scss postcss.config.js## Compile stylesheets
	$(POSTCSS) $< --output $@

$(DIST)/index.html: $(SRC)/index.html .posthtmlrc $(DIST)/missing-poster.min.js $(DIST)/missing-poster.min.css
	$(POSTHTML) $< $(POSTHTML_FLAGS) -o $@

node_modules: package.json
	$(NPM) prune && $(NPM) install

format: $(SRC_HTML) $(SRC_JS) $(SRC_SCSS) $(SRC_MD) ## Format code with prettier
	$(PRETTIER) $^  $(PRETTIER_FLAGS)

lint: $(SRC_JS) ## Lint code with eslint
	$(ESLINT) $^ $(ESLINT_FLAGS)

clean:
	$(RM) -rf $(DIST)

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'

.PHONY: all clean help format lint
