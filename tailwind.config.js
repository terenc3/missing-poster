/*
 * SPDX-FileCopyrightText: 2022 Benjamin Kahlau
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

module.exports = {
    content: ['./src/index.html']
}
