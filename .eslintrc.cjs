module.exports = {
    "env": {
        "node": true,
        "es6": true,
        "browser": true
    },
    "globals": {
        "QRCode": true
    },
    "extends": [
        "eslint:recommended",
        "prettier"
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    }
}
